package com.atlassian.bitbucket.linky.hosting;

import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.Optional;

public interface Hosting {

    enum Type {CLOUD, SERVER}

    @NotNull
    URI getBaseUri();

    @NotNull
    Optional<URI> getBaseRestUri();

    @NotNull
    Type getType();

    default boolean isBitbucket() {
        return getType() == Type.CLOUD || getType() == Type.SERVER;
    }
}
