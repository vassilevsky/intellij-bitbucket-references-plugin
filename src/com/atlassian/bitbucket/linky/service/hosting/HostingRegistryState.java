package com.atlassian.bitbucket.linky.service.hosting;

import com.atlassian.bitbucket.linky.hosting.Hosting;
import com.intellij.util.containers.HashMap;
import com.intellij.util.containers.HashSet;
import com.intellij.util.xmlb.annotations.AbstractCollection;
import com.intellij.util.xmlb.annotations.MapAnnotation;
import com.intellij.util.xmlb.annotations.Tag;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class HostingRegistryState {
    @Tag("hostings")
    @AbstractCollection(surroundWithTag = false, elementTag = "hosting")
    private Collection<HostingBean> hostings = new HashSet<>();

    @Tag("accessMap")
    @MapAnnotation(surroundWithTag = false, surroundKeyWithTag = false, surroundValueWithTag = false)
    private Map<HostingAccessBean, String> accessMap = new HashMap<>();

    public static HostingRegistryState with(Map<HostingAccessBean, Hosting> servers) {
        Map<Hosting, HostingBean> hostingToBean = servers.values().stream()
                .distinct()
                .collect(toMap(
                        identity(),
                        h -> HostingBean.of(UUID.randomUUID().toString(), h)));

        HostingRegistryState state = new HostingRegistryState();
        state.hostings = hostingToBean.values();
        state.accessMap = servers.entrySet().stream()
                .collect(toMap(
                        Map.Entry::getKey,
                        e -> hostingToBean.get(e.getValue()).getId()));

        return state;
    }

    Map<HostingAccessBean, Hosting> getAccessMap() {
        Map<String, Hosting> hostingById = hostings.stream().collect(toMap(HostingBean::getId, HostingBean::toHosting));

        return accessMap.entrySet().stream()
                .collect(toMap(
                        Map.Entry::getKey,
                        e -> hostingById.get(e.getValue())));
    }
}
