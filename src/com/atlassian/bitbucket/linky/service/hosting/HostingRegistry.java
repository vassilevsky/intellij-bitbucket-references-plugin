package com.atlassian.bitbucket.linky.service.hosting;

import com.atlassian.bitbucket.linky.hosting.Hosting;
import com.atlassian.bitbucket.linky.util.UriScheme;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.List;
import java.util.Optional;

public interface HostingRegistry {
    @NotNull
    Optional<Hosting> getBitbucketCloud(@NotNull String host);

    @NotNull
    Optional<Hosting> getBitbucketServer(@NotNull UriScheme scheme,
                                         @NotNull String host,
                                         int port,
                                         @NotNull String path);

    @NotNull
    List<Hosting> getKnownHostings();

    @NotNull
    Optional<Hosting> registerBitbucketCloud(@NotNull String host);

    @NotNull
    Optional<Hosting> registerBitbucketServer(@NotNull UriScheme scheme,
                                              @NotNull String host,
                                              int port,
                                              @NotNull String path,
                                              @NotNull URI applicationUri);
}
