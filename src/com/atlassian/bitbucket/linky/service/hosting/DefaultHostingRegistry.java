package com.atlassian.bitbucket.linky.service.hosting;

import com.atlassian.bitbucket.linky.hosting.DefaultHosting;
import com.atlassian.bitbucket.linky.hosting.Hosting;
import com.atlassian.bitbucket.linky.util.UriScheme;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.components.StoragePathMacros;
import com.intellij.openapi.diagnostic.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.util.*;
import java.util.function.Supplier;

import static com.atlassian.bitbucket.linky.Constants.BITBUCKET_CLOUD_HOST;
import static com.atlassian.bitbucket.linky.Constants.LOGGER_NAME;
import static com.atlassian.bitbucket.linky.util.UriScheme.HTTPS;
import static com.atlassian.bitbucket.linky.util.Uris.buildUri;
import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableMap;
import static java.util.Optional.*;
import static java.util.stream.Collectors.toList;

@State(name = "BitbucketHostingRegistry",
        storages = {
                @Storage(id = "file", file = StoragePathMacros.APP_CONFIG + "/bitbucket-hosting-registry.xml")
        }
)
public class DefaultHostingRegistry implements HostingRegistry, PersistentStateComponent<HostingRegistryState> {

    private static final Logger log = Logger.getInstance(LOGGER_NAME);

    private final Map<HostingAccessBean, Hosting> accessMap = new HashMap<>();

    @NotNull
    @Override
    public Optional<Hosting> getBitbucketCloud(@NotNull String host) {
        return ofNullable(accessMap.get(HostingAccessBean.of(host)));
    }

    @NotNull
    @Override
    public Optional<Hosting> getBitbucketServer(@NotNull UriScheme scheme,
                                                @NotNull String host,
                                                int port,
                                                @NotNull String path) {
        return ofNullable(accessMap.get(HostingAccessBean.of(scheme, host, port, path)));
    }

    @NotNull
    @Override
    public List<Hosting> getKnownHostings() {
        return accessMap.values().stream()
                .distinct()
                .collect(toList());
    }

    @NotNull
    @Override
    public Optional<Hosting> registerBitbucketCloud(@NotNull String host) {
        return getOrRegisterHosting(accessMap, HostingAccessBean.of(host),
                () -> createBitbucketCloud(host.toLowerCase(Locale.ROOT)));
    }

    @NotNull
    @Override
    public Optional<Hosting> registerBitbucketServer(@NotNull UriScheme scheme,
                                                     @NotNull String host,
                                                     int port,
                                                     @NotNull String path,
                                                     @NotNull URI applicationUri) {

        return getOrRegisterHosting(accessMap, HostingAccessBean.of(scheme, host, port, path),
                () -> createBitbucketServer(applicationUri));
    }

    @Nullable
    @Override
    public HostingRegistryState getState() {
        return accessMap.isEmpty() ? null : HostingRegistryState.with(unmodifiableMap(accessMap));
    }

    @Override
    public void loadState(HostingRegistryState state) {
        accessMap.clear();
        accessMap.putAll(state.getAccessMap());
    }

    private Optional<Hosting> createBitbucketCloud(String host) {
        return buildUri(of(HTTPS), of(host), empty(), of("/"), emptyMap(), empty(),
                e -> log.warn("Failed to build URI for Bitbucket Cloud", e))
                .flatMap(uri -> resolveCloudBaseRestUri(uri)
                        .map(restUri -> DefaultHosting.cloud()
                                .baseUri(uri)
                                .baseRestUri(restUri)
                                .build()));
    }

    private Optional<URI> resolveCloudBaseRestUri(URI baseUri) {
        if (BITBUCKET_CLOUD_HOST.equals(baseUri.getHost())) {
            return buildUri(of(HTTPS), of("api.bitbucket.org"), empty(), of("/2.0/"), emptyMap(), empty(),
                    e -> log.warn("Failed to build REST URI for Bitbucket Cloud", e));
        } else {
            return of(baseUri.resolve("/!api/2.0/"));
        }
    }

    private Optional<Hosting> createBitbucketServer(URI applicationUri) {
        return of(DefaultHosting.server()
                .baseUri(applicationUri)
                .baseRestUri(applicationUri.resolve("/rest/api/latest/"))
                .build());
    }

    private <K, H> Optional<H> getOrRegisterHosting(Map<K, H> storage, K key, Supplier<Optional<H>> hostingSupplier) {
        Optional<H> hosting = ofNullable(storage.get(key));
        if (!hosting.isPresent()) {
            hosting = hostingSupplier.get();
            hosting.ifPresent(v -> {
                storage.put(key, v);
                log.debug(format("Created hosting '%s'", v));
            });
        }
        return hosting;
    }
}
