package com.atlassian.bitbucket.linky.service.hosting;

import com.atlassian.bitbucket.linky.util.UriScheme;
import com.intellij.util.xmlb.annotations.Tag;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Locale;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class HostingAccessBean {

    @Tag
    private UriScheme scheme;
    @Tag
    private String host;
    @Tag
    private int port;
    @Tag
    private String path;

    public static HostingAccessBean of(@NotNull String host) {
        return of(null, host, -1, null);
    }

    public static HostingAccessBean of(@Nullable UriScheme scheme, @NotNull String host, int port, @Nullable String path) {
        HostingAccessBean hostingAccessBean = new HostingAccessBean();
        hostingAccessBean.scheme = scheme;
        hostingAccessBean.host = requireNonNull(host, "host").toLowerCase(Locale.ROOT);
        hostingAccessBean.port = port;

        if (path != null) {
            path = path.toLowerCase(Locale.ROOT);
        }
        hostingAccessBean.path = path;

        return hostingAccessBean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HostingAccessBean that = (HostingAccessBean) o;
        return port == that.port &&
                Objects.equals(scheme, that.scheme) &&
                Objects.equals(host, that.host) &&
                Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheme, host, port, path);
    }
}
