package com.atlassian.bitbucket.linky.util;

import java.util.Arrays;
import java.util.Optional;

public enum UriScheme {
    GIT("git"),
    HTTP("http"),
    HTTPS("https"),
    SSH("ssh");

    private final String presentation;

    UriScheme(String presentation) {
        this.presentation = presentation;
    }

    public static Optional<UriScheme> forName(String presentation) {
        return Arrays.stream(values())
                .filter(v -> v.presentation.equals(presentation))
                .findAny();
    }

    @Override
    public String toString() {
        return presentation;
    }
}