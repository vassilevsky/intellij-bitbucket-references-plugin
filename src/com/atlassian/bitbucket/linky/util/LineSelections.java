package com.atlassian.bitbucket.linky.util;

import com.atlassian.bitbucket.linky.LinesSelection;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class LineSelections {

    private LineSelections() {
        throw new UnsupportedOperationException(getClass().getSimpleName() + " only contains static utility methods " +
                "and should not be instantiated.");
    }

    public static Optional<String> collectLinesSelections(@NotNull List<LinesSelection> linesSelections,
                                                          @NotNull CharSequence prefix,
                                                          @NotNull CharSequence selectionsDelimiter,
                                                          @NotNull CharSequence intervalSign,
                                                          @NotNull CharSequence suffix) {
        List<LinesSelection> sortedDistinct = linesSelections.stream()
                .distinct()
                .sorted()
                .collect(toList());

        List<LinesSelection> selections = new ArrayList<>();
        LinesSelection currentSelection = null;

        for (LinesSelection s : sortedDistinct) {
            if (currentSelection == null) {
                currentSelection = s;
            } else {
                int currentStart = currentSelection.getFirstLineNumber();
                int currentEnd = currentSelection.getLastLineNumber();
                int start = s.getFirstLineNumber();
                int end = s.getLastLineNumber();

                if (currentEnd >= start - 1) {
                    // if adjacent or overlaps, extend current selection
                    currentSelection = LinesSelection.of(min(currentStart, start), max(currentEnd, end));
                } else {
                    // if not, push current selection to result list and update with new value
                    selections.add(currentSelection);
                    currentSelection = s;
                }
            }
        }
        // push last current selection
        if (currentSelection != null) {
            selections.add(currentSelection);
        }

        String serialized = selections.stream()
                .map(selection -> {
                    int start = selection.getFirstLineNumber();
                    int end = selection.getLastLineNumber();
                    if (start < end) {
                        return String.format("%d%s%d", start, intervalSign, end);
                    }
                    return String.valueOf(start);
                })
                .collect(joining(selectionsDelimiter, prefix, suffix));
        return serialized.length() == prefix.length() + suffix.length() ? empty() : of(serialized);
    }
}
