package com.atlassian.bitbucket.linky.discovery;

import com.atlassian.bitbucket.linky.service.blame.BlameLineService;
import com.atlassian.bitbucket.linky.service.hosting.HostingRegistry;
import com.atlassian.bitbucket.linky.util.Uris;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.diagnostic.Logger;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.bitbucket.linky.Constants.*;
import static java.lang.String.format;
import static java.util.Optional.ofNullable;

public abstract class AbstractRemoteAnalyzer implements VcsRemoteAnalyzer {

    protected static final Logger log = Logger.getInstance(LOGGER_NAME);

    protected final BlameLineService blameLineService;
    protected final HostingRegistry hostingRegistry;

    protected AbstractRemoteAnalyzer(HostingRegistry hostingRegistry, BlameLineService blameLineService) {
        this.hostingRegistry = hostingRegistry;
        this.blameLineService = blameLineService;
    }

    // TODO switch to API check
    protected boolean containsBitbucketCloudHost(String string) {
        return string.contains(BITBUCKET_CLOUD_HOST) || string.contains(BITBUCKET_CLOUD_DEV_HOST);
    }

    protected String getCurrentVcsReference(@NotNull Repository repository) {
        return ofNullable(repository.getCurrentRevision())
                .orElseGet(() -> ofNullable(repository.getCurrentBranchName())
                        .orElse(""));
    }

    @NotNull
    protected Optional<URI> parseRemote(@NotNull String remote) {
        Optional<URI> maybeUri = Uris.parseRemote(remote, e ->
                log.warn(format("Failed to create URI from provided URL '%s'", remote), e));

        maybeUri.ifPresent(uri ->
                log.debug(format("Parsed URI parameters: host '%s', path '%s'", uri.getHost(), uri.getPath())));

        return maybeUri;
    }
}
