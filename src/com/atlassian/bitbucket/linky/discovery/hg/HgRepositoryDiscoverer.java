package com.atlassian.bitbucket.linky.discovery.hg;

import com.atlassian.bitbucket.linky.discovery.VcsRemoteAnalyzer;
import com.atlassian.bitbucket.linky.discovery.VcsRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.atlassian.bitbucket.linky.util.Optionals;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.zmlx.hg4idea.repo.HgConfig;
import org.zmlx.hg4idea.repo.HgRepository;
import org.zmlx.hg4idea.repo.HgRepositoryManager;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;

public class HgRepositoryDiscoverer implements VcsRepositoryDiscoverer {

    private final HgRepositoryManager hgRepoManager;
    private final VcsRemoteAnalyzer vcsRemoteAnalyzer;

    public HgRepositoryDiscoverer(@NotNull HgRepositoryManager hgRepoManager,
                                  @NotNull VcsRemoteAnalyzer vcsRemoteAnalyzer) {
        this.hgRepoManager = requireNonNull(hgRepoManager, "hgRepoManager");
        this.vcsRemoteAnalyzer = requireNonNull(vcsRemoteAnalyzer, "vcsRemoteAnalyzer");
    }

    @NotNull
    @Override
    public Map<VirtualFile, BitbucketRepository> discoverBitbucketRepositories() {
        return hgRepoManager.getRepositories().stream()
                .map(repo -> remotesOfRepo(repo).stream()
                        .map(remote -> vcsRemoteAnalyzer.bitbucketRepositoryFor(repo, remote))
                        .flatMap(Optionals::toStream)
                        .findFirst())
                .flatMap(Optionals::toStream)
                .collect(Collectors.toMap(BitbucketRepository::getRootDirectory, identity()));
    }

    @NotNull
    private List<String> remotesOfRepo(@NotNull HgRepository hgRepo) {
        HgConfig repoConfig = hgRepo.getRepositoryConfig();
        String mainRemote = repoConfig.getDefaultPath();

        return repoConfig.getPaths().stream()
                .sorted((path1, path2) ->
                        // move main remote to the beginning, do not reorder other remotes
                        Optional.ofNullable(mainRemote)
                                .map(main -> main.equals(path1) ? -1 : (main.equals(path2) ? 1 : 0))
                                .orElse(0))
                .collect(toList());
    }
}
