package com.atlassian.bitbucket.linky.discovery;


import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public interface VcsRemoteAnalyzer {

    @NotNull
    Optional<BitbucketRepository> bitbucketRepositoryFor(@NotNull Repository repository, @NotNull String remote);
}
