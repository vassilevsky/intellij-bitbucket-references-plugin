package com.atlassian.bitbucket.linky.discovery;

import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public interface VcsRepositoryDiscoverer {

    @NotNull
    Map<VirtualFile, BitbucketRepository> discoverBitbucketRepositories();
}
