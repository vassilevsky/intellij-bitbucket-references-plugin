package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.atlassian.bitbucket.linky.service.BitbucketRepositoryService;
import com.intellij.ide.BrowserUtil;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.vcs.AbstractVcs;
import com.intellij.openapi.vcs.FileStatus;
import com.intellij.openapi.vcs.FileStatusManager;
import com.intellij.openapi.vcs.ProjectLevelVcsManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.ui.awt.RelativePoint;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.util.Optional;
import java.util.function.Supplier;

import static com.atlassian.bitbucket.linky.Constants.BITBUCKET_ICON;
import static com.atlassian.bitbucket.linky.Constants.LOGGER_NAME;
import static java.lang.String.format;
import static java.util.Optional.ofNullable;

public abstract class AbstractBitbucketReferenceAction extends DumbAwareAction {

    private static final Logger log = Logger.getInstance(LOGGER_NAME);

    private final String name;

    public AbstractBitbucketReferenceAction() {
        this(null, null);
    }

    public AbstractBitbucketReferenceAction(@Nullable String text, @Nullable String description) {
        super(text, description, BITBUCKET_ICON);
        this.name = getClass().getSimpleName();
    }

    @Override
    public final void actionPerformed(AnActionEvent e) {
        DataContext dataContext = e.getDataContext();

        Boolean performed = ofNullable(e.getProject())
                .map(project -> ofNullable(CommonDataKeys.VIRTUAL_FILE.getData(dataContext))
                        .map(file -> ofNullable(ProjectLevelVcsManager.getInstance(project).getVcsFor(file))
                                .map(vcs -> getLinky(project, file)
                                        .map(linky -> {
                                            actionPerformed(e, project, file, linky, vcs);
                                            return true;
                                        }).orElseGet(loggingSupplier("Linky not available"))
                                ).orElseGet(loggingSupplier("VCS not detected for selected file"))
                        ).orElseGet(loggingSupplier("Virtual file not defined"))
                ).orElseGet(loggingSupplier("Project not defined"));

        if (!performed) {
            showPopupMessage(e.getProject(), MessageType.WARNING,
                    format("Action '%s' could not be performed. Please check IDE logs.", e.getPresentation().getText()));
        }
    }

    @Override
    public final void update(AnActionEvent event) {
        Project project = event.getProject();
        VirtualFile file = CommonDataKeys.VIRTUAL_FILE.getData(event.getDataContext());

        if (project != null &&
                file != null &&
                isFileWithProperStatus(project, file)) {
            Optional<BitbucketLinky> maybeLinky = getLinky(project, file);

            boolean enabled = maybeLinky
                    .map(linky -> couldActionBeEnabled(project, file))
                    .orElse(false);

            log.debug(format("%s %s action for file '%s'", enabled ? "Enabled" : "Disabled", name, file));
            event.getPresentation().setEnabledAndVisible(enabled);
        } else {
            event.getPresentation().setEnabledAndVisible(false);
        }
    }

    protected abstract void actionPerformed(@NotNull AnActionEvent event,
                                            @NotNull Project project,
                                            @NotNull VirtualFile file,
                                            @NotNull BitbucketLinky linky,
                                            @NotNull AbstractVcs vcs);

    protected boolean couldActionBeEnabled(@NotNull Project project, @NotNull VirtualFile file) { // TODO parameter list
        return true;
    }

    protected void openInBrowser(Project project, URI uri) {
        BrowserUtil.browse(uri.toASCIIString());
        showPopupMessage(project, MessageType.INFO, uri.toString() + " is now being opened in browser");
    }

    protected void showPopupMessage(@Nullable Project project, @NotNull MessageType messageType, @NotNull String message) {
        ofNullable(WindowManager.getInstance().getStatusBar(project))
                .ifPresent(statusBar ->
                        JBPopupFactory.getInstance()
                                .createHtmlTextBalloonBuilder(message, messageType, null)
                                .setFadeoutTime(7500)
                                .createBalloon()
                                .show(RelativePoint.getCenterOf(statusBar.getComponent()),
                                        Balloon.Position.atRight));
    }

    private Optional<BitbucketLinky> getLinky(@NotNull Project project, @NotNull VirtualFile file) {
        return ofNullable(ServiceManager.getService(project, BitbucketRepositoryService.class))
                .flatMap(service -> service.getBitbucketLinky(file));
    }

    private boolean isFileWithProperStatus(@NotNull Project project, @NotNull VirtualFile file) {
        FileStatusManager fileStatusManager = FileStatusManager.getInstance(project);
        FileStatus status = fileStatusManager.getStatus(file);
        return status != FileStatus.UNKNOWN && status != FileStatus.IGNORED;
    }

    @NotNull
    private Supplier<Boolean> loggingSupplier(String message) {
        return () -> {
            log.warn(message);
            return false;
        };
    }

}
