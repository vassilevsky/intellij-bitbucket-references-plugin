package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.action.reference.OpenCommitInBitbucketAction;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.vcs.annotate.AnnotationGutterActionProvider;
import com.intellij.openapi.vcs.annotate.FileAnnotation;
import org.jetbrains.annotations.NotNull;

public class DefaultAnnotationGutterActionProvider implements AnnotationGutterActionProvider {

    @NotNull
    @Override
    public AnAction createAction(@NotNull FileAnnotation annotation) {
        return new OpenCommitInBitbucketAction(annotation);
    }
}
