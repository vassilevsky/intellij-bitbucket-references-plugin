package com.atlassian.bitbucket.linky.repository;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.atlassian.bitbucket.linky.hosting.Hosting;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.Optional;

public interface BitbucketRepository extends BitbucketLinky {

    enum VcsType {GIT, MERCURIAL}

    @NotNull
    Hosting getHosting();

    @NotNull
    VcsType getVcsType();

    @NotNull
    VirtualFile getRootDirectory();

    @NotNull
    Optional<String> getName();

    @NotNull
    Optional<String> getRemoteName();

    @NotNull
    URI getUri();
}
