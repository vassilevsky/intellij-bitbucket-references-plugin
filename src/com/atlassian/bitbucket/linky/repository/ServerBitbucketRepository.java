package com.atlassian.bitbucket.linky.repository;

import com.atlassian.bitbucket.linky.LinesSelection;
import com.atlassian.bitbucket.linky.util.Uris;
import com.google.common.collect.ImmutableMap;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.atlassian.bitbucket.linky.util.LineSelections.collectLinesSelections;
import static com.atlassian.bitbucket.linky.util.Uris.buildUri;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class ServerBitbucketRepository extends AbstractBitbucketRepository<ServerBitbucketRepository.Builder> {
    private static final String SOURCE_VIEW_RELATIVE_PATH = "browse";
    private static final String COMMITS_VIEW_RELATIVE_PATH = "commits";
    private static final String PULL_REQUEST_RELATIVE_PATH = "pull-requests?create";
    private static final String QUERY_PARAM_FORMAT = "%s&sourceBranch=%s";

    private ServerBitbucketRepository(Builder builder) {
        super(builder);
    }

    public static ServerBitbucketRepository.Builder builder(@NotNull VirtualFile rootDirectory) {
        return new ServerBitbucketRepository.Builder(rootDirectory);
    }

    @NotNull
    @Override
    public URI getCommitViewUri(@NotNull String commitReference, @NotNull VirtualFile file, int lineNumber) {
        return blameLineService.getBlameLineReference(file, lineNumber)
                // Bitbucket Server doesn't support line selection in the commit view
                .map(pair -> uri.resolve(format("%s/%s#%s", COMMITS_VIEW_RELATIVE_PATH, commitReference, pair.first)))
                .orElseGet(() -> uri.resolve(format("%s/%s", COMMITS_VIEW_RELATIVE_PATH, commitReference)));
    }

    @NotNull
    @Override
    public URI getSourceViewUri() {
        return uri.resolve(SOURCE_VIEW_RELATIVE_PATH);
    }

    @NotNull
    @Override
    public URI getSourceViewUri(@NotNull VirtualFile file, @NotNull List<LinesSelection> linesSelections) {
        String relativePath = format("%s/%s", SOURCE_VIEW_RELATIVE_PATH, getRelativeFilePath(file));
        Optional<String> fragment = collectLinesSelections(linesSelections, "", ",", "-", "");

        return buildUri(empty(), empty(), empty(),
                of(relativePath), ImmutableMap.of("at", getCurrentVcsReference()), fragment,
                e -> log.error(format("Failed to resolve a relative URI for the file '%s'", file), e))
                .map(uri::resolve)
                .orElse(uri);
    }

    @NotNull
    @Override
    public URI getPullRequestUri() {
        String branchName = vcsRepository.getCurrentBranchName();
        Objects.requireNonNull(branchName, "branch name should not be null");
        branchName = Uris.quote(branchName);
        return uri.resolve(format(QUERY_PARAM_FORMAT, PULL_REQUEST_RELATIVE_PATH, branchName));
    }

    public static class Builder extends AbstractBitbucketRepository.Builder<Builder> {

        protected Builder(@NotNull VirtualFile rootDirectory) {
            super(rootDirectory);
        }

        @NotNull
        public ServerBitbucketRepository build() {
            return new ServerBitbucketRepository(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}
