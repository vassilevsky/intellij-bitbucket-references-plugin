package com.atlassian.bitbucket.linky.repository;

import com.atlassian.bitbucket.linky.LinesSelection;
import com.atlassian.bitbucket.linky.util.Uris;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.atlassian.bitbucket.linky.util.LineSelections.collectLinesSelections;
import static com.atlassian.bitbucket.linky.util.Uris.buildUri;
import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class CloudBitbucketRepository extends AbstractBitbucketRepository<CloudBitbucketRepository.Builder> {
    private static final String SOURCE_VIEW_RELATIVE_PATH = "src";
    private static final String COMMITS_VIEW_RELATIVE_PATH = "commits";
    private static final String PULL_REQUEST_RELATIVE_PATH = "pull-requests/new";
    private static final String QUERY_PARAM_FORMAT = "%s?source=%s";

    private CloudBitbucketRepository(Builder builder) {
        super(builder);
    }

    public static CloudBitbucketRepository.Builder builder(@NotNull VirtualFile rootDirectory) {
        return new Builder(rootDirectory);
    }

    @NotNull
    @Override
    public URI getCommitViewUri(@NotNull String commitReference, @NotNull VirtualFile file, int lineNumber) {
        return blameLineService.getBlameLineReference(file, lineNumber)
                .map(pair -> uri.resolve(format("%s/%s#L%sT%d", COMMITS_VIEW_RELATIVE_PATH, commitReference, pair.first, pair.second)))
                .orElseGet(() -> uri.resolve(format("%s/%s", COMMITS_VIEW_RELATIVE_PATH, commitReference)));
    }

    @NotNull
    @Override
    public URI getSourceViewUri() {
        return uri.resolve(SOURCE_VIEW_RELATIVE_PATH);
    }

    @NotNull
    @Override
    public URI getSourceViewUri(@NotNull VirtualFile file, @NotNull List<LinesSelection> linesSelections) {
        String relativePath = format("%s/%s/%s",
                SOURCE_VIEW_RELATIVE_PATH, getCurrentVcsReference(), getRelativeFilePath(file));
        Optional<String> fragment = collectLinesSelections(linesSelections, file.getName() + "-", ",", ":", "");

        return buildUri(empty(), empty(), empty(), of(relativePath), emptyMap(), fragment,
                e -> log.error(format("Failed to resolve a relative URI for the file '%s'", file), e))
                .map(uri::resolve)
                .orElse(uri);
    }

    @NotNull
    @Override
    public URI getPullRequestUri() {
        String branchName = vcsRepository.getCurrentBranchName();
        Objects.requireNonNull(branchName, "branch name should not be null");
        branchName = Uris.quote(branchName);
        return uri.resolve(format(QUERY_PARAM_FORMAT, PULL_REQUEST_RELATIVE_PATH, branchName));
    }

    public static class Builder extends AbstractBitbucketRepository.Builder<Builder> {

        protected Builder(@NotNull VirtualFile rootDirectory) {
            super(rootDirectory);
        }

        @NotNull
        public CloudBitbucketRepository build() {
            return new CloudBitbucketRepository(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}
