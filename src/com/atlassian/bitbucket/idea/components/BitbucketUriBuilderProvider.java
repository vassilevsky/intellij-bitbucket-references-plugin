package com.atlassian.bitbucket.idea.components;

import com.atlassian.bitbucket.idea.BitbucketUriBuilder;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.Nullable;

public interface BitbucketUriBuilderProvider {

    @Nullable
    BitbucketUriBuilder getBitbucketUriBuilder(VirtualFile file);
}
