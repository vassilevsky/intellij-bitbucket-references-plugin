package com.atlassian.bitbucket.idea.pullrequest;

import java.util.List;

public interface PullRequestProvider {

    List<PullRequest> getOpenPullRequests();
}
