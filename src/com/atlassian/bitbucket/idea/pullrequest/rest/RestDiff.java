package com.atlassian.bitbucket.idea.pullrequest.rest;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;

import java.util.ArrayList;
import java.util.List;

public class RestDiff {
    private List<Diff> diffs;

    public List<Segment> getAllSegments() {
        ArrayList<Segment> segments = new ArrayList<Segment>();
        if (diffs != null) {
            for (final Diff diff : diffs) {
                for (Hunk hunk : diff.hunks) {
                    segments.addAll(hunk.segments);
                }
            }
        }
        return segments;
    }

    public List<RestComment> getComments() {
        ArrayList<RestComment> comments = new ArrayList<RestComment>();

        if (diffs != null) {
            for (final Diff diff : diffs) {
                List<RestComment> diffComments = diff.lineComments;
                if (diffComments != null) {
                    for (Hunk hunk : diff.hunks) {
                        for (final Segment segment : hunk.segments) {
                            for (final Line line : segment.lines) {
                                final List<Long> commentIds = line.commentIds;
                                comments.addAll(FluentIterable.from(diffComments)
                                        .filter(new Predicate<RestComment>() {
                                            @Override
                                            public boolean apply(RestComment restComment) {
                                                return commentIds != null && commentIds.contains(restComment.getId());
                                            }
                                        })
                                        .transform(new Function<RestComment, RestComment>() {
                                            @Override
                                            public RestComment apply(RestComment restComment) {
                                                RestCommentAnchor anchor = RestCommentAnchor.build(
                                                        line.destination,
                                                        "TO",
                                                        segment.type,
                                                        diff.getRelativeFilePath());
                                                restComment.setAnchor(anchor);
                                                return restComment;
                                            }
                                        })
                                        .toList());
                            }
                        }
                    }
                }
            }
        }

        return comments;
    }

    private static class Diff {
        private Destination destination;
        private List<Hunk> hunks;
        private List<RestComment> lineComments;

        public String getRelativeFilePath() {
            return destination == null ? null : destination.toString;
        }
    }

    private static class Destination {
        private String toString;
    }

    private static class Hunk {
        private List<Segment> segments;
    }

    public static class Segment {
        private String type;
        private List<Line> lines;

        public String getType() {
            return type;
        }

        public List<Line> getLines() {
            return lines;
        }
    }

    public static class Line {
        private int source;
        private int destination;
        private List<Long> commentIds;

        public int getSource() {
            return source;
        }

        public int getDestination() {
            return destination;
        }

        public List<Long> getCommentIds() {
            return commentIds;
        }
    }
}
