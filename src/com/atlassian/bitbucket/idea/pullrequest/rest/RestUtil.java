package com.atlassian.bitbucket.idea.pullrequest.rest;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

public class RestUtil {

    private static final Gson gson = initGson();

    public static List<RestChangedFile> parseChangedFiles(JsonObject jsonObject) {
        Type listType = new TypeToken<List<RestChangedFile>>() {
        }.getType();

        return gson.fromJson(getPageContent(jsonObject), listType);
    }

    public static List<RestComment> parseComments(JsonObject jsonObject) {
        Type listType = new TypeToken<List<RestComment>>() {
        }.getType();

        return gson.fromJson(getPageContent(jsonObject), listType);
    }

    public static RestDiff parseDiff(JsonObject jsonObject) {
        // TODO currently we expect only one diff there
        return gson.fromJson(jsonObject, RestDiff.class);
    }

    public static RestPullRequest parsePullRequest(JsonObject jsonObject) {
        return gson.fromJson(jsonObject, RestPullRequest.class);
    }

    public static List<RestPullRequest> parsePullRequests(JsonObject jsonObject) {
        Type listType = new TypeToken<List<RestPullRequest>>() {
        }.getType();

        return gson.fromJson(getPageContent(jsonObject), listType);
    }

    public static RestUser parseUser(JsonObject jsObject) {
        return gson.fromJson(jsObject, RestUser.class);
    }


    public static String writeComment(RestPostComment postComment) {
        return gson.toJson(postComment, RestPostComment.class);
    }

    private static JsonArray getPageContent(JsonObject jsonObject) {
        return jsonObject.getAsJsonArray("values");
    }

    @NotNull
    private static Gson initGson() {
        GsonBuilder builder = new GsonBuilder();

        // Register an adapter to manage the date types as long values
        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type type, JsonDeserializationContext ctx) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        });

        return builder.create();
    }
}
