package com.atlassian.bitbucket.idea.pullrequest.rest;

import com.atlassian.bitbucket.idea.pullrequest.comment.ui.CommentsListener;
import com.atlassian.bitbucket.idea.rest.RestClient;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.JsonObject;
import com.intellij.openapi.util.Pair;
import com.intellij.util.messages.MessageBus;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class DefaultPullRequestRestApi implements PullRequestRestApi {
    private static final String PULL_REQUESTS_PATH = "pull-requests";
    private static final String PULL_REQUEST_PATH_PATTERN = "pull-requests/%d";
    private static final String PULL_REQUEST_CHANGES_PATH_PATTERN = "pull-requests/%d/changes";
    private static final String PULL_REQUEST_DIFF_PATH_PATTERN = "pull-requests/%d/diff/%s?markup=true";
    private static final String PULL_REQUEST_POST_COMMENT_PATH_PATTERN = "pull-requests/%d/comments";
    private static final String PULL_REQUEST_APPROVE_PATH_PATTERN = "pull-requests/%d/approve";

    private final LoadingCache<Boolean, List<RestPullRequest>> pullRequests = createCache(
            new CacheLoader<Boolean, List<RestPullRequest>>() {
                @Override
                public List<RestPullRequest> load(Boolean dummy) throws Exception {
                    JsonObject response = restClient.get(PULL_REQUESTS_PATH);
                    return RestUtil.parsePullRequests(response);
                }
            });

    private final LoadingCache<Long, List<RestChangedFile>> changes = createCache(
            new CacheLoader<Long, List<RestChangedFile>>() {
                @Override
                public List<RestChangedFile> load(Long pullRequestId) throws IOException {
                    JsonObject response = restClient.get(String.format(PULL_REQUEST_CHANGES_PATH_PATTERN, pullRequestId));
                    return RestUtil.parseChangedFiles(response);
                }
            });

    private final LoadingCache<Pair<Long, String>, RestDiff> diffs = createCache(
            new CacheLoader<Pair<Long, String>, RestDiff>() {
                public RestDiff load(Pair<Long, String> pair) throws IOException {
                    JsonObject response = restClient.get(String.format(PULL_REQUEST_DIFF_PATH_PATTERN, pair.getFirst(), pair.getSecond()));
                    return RestUtil.parseDiff(response);
                }
            });

    private final RestClient restClient;
    private final MessageBus messageBus;

    public DefaultPullRequestRestApi(RestClient restClient, MessageBus messageBus) {
        this.restClient = restClient;
        this.messageBus = messageBus;
    }

    @Override
    public List<RestPullRequest> getPullRequests() throws IOException {
        try {
            return pullRequests.get(false); // dummy key
        } catch (ExecutionException e) {
            throw (IOException) e.getCause();
        }
    }

    @Override
    public RestPullRequest getPullRequest(long pullRequestId) throws IOException {
        JsonObject response = restClient.get(String.format(PULL_REQUEST_PATH_PATTERN, pullRequestId));
        return RestUtil.parsePullRequest(response);
    }

    @Override
    public List<RestChangedFile> getChanges(long pullRequestId) throws IOException {
        try {
            return changes.get(pullRequestId);
        } catch (ExecutionException e) {
            throw (IOException) e.getCause();
        }
    }

    @Override
    public List<RestComment> getComments(long pullRequestId, String path) throws IOException {
        try {
            return diffs.get(Pair.pair(pullRequestId, path)).getComments();
        } catch (ExecutionException e) {
            throw (IOException) e.getCause();
        }
    }

    @Override
    public RestDiff getDiff(long pullRequestId, String path) throws IOException {
        try {
            return diffs.get(Pair.pair(pullRequestId, path));
        } catch (ExecutionException e) {
            throw (IOException) e.getCause();
        }
    }

    @Override
    public void postComment(long pullRequestId, RestPostComment postComment) throws IOException {
        String jsonBody = RestUtil.writeComment(postComment);
        restClient.post(String.format(PULL_REQUEST_POST_COMMENT_PATH_PATTERN, pullRequestId), jsonBody);
        diffs.invalidateAll();
        CommentsListener commentsListener = messageBus.syncPublisher(CommentsListener.COMMENTS_UPDATED_TOPIC);
        commentsListener.commentsUpdated();
    }

    @Override
    public boolean approve(long pullRequestId) throws IOException {
        int resultCode = restClient.post(String.format(PULL_REQUEST_APPROVE_PATH_PATTERN, pullRequestId), "");
        return resultCode == 200;
    }

    @Override
    public boolean unapprove(long pullRequestId) throws IOException {
        int resultCode = restClient.delete(String.format(PULL_REQUEST_APPROVE_PATH_PATTERN, pullRequestId));
        return resultCode == 200;
    }

    private static <K, V> LoadingCache<K, V> createCache(CacheLoader<K, V> cacheLoader) {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .maximumSize(10)
                .build(cacheLoader);
    }
}
