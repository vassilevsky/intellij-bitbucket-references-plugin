package com.atlassian.bitbucket.idea.pullrequest;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.idea.pullrequest.rest.PullRequestRestApi;
import com.atlassian.bitbucket.idea.pullrequest.rest.RestPullRequest;
import com.atlassian.bitbucket.idea.pullrequest.rest.RestUser;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.intellij.openapi.diagnostic.Logger;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.List;

public class DefaultPullRequestProvider implements PullRequestProvider {
    private static final Logger log = Logger.getInstance(Constants.LOGGER_NAME);

    private static final Function<RestPullRequest, PullRequest> CONVERT_FROM_REST = new Function<RestPullRequest, PullRequest>() {
        @Override
        public PullRequest apply(RestPullRequest restPullRequest) {
            RestUser author = restPullRequest.getAuthor();
            return DefaultPullRequest.build(
                    restPullRequest.getId(),
                    DefaultUser.builder()
                            .displayName(author.getDisplayName())
                            .slug(author.getSlug())
                            .userUri(URI.create(author.getLink()))
                            .build(),
                    restPullRequest.getCreatedDate(),
                    restPullRequest.getTitle()
            );
        }
    };

    private final PullRequestRestApi restApi;

    public DefaultPullRequestProvider(PullRequestRestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public List<PullRequest> getOpenPullRequests() {
        try {
            List<RestPullRequest> pullRequests = restApi.getPullRequests();
            return FluentIterable.from(pullRequests)
                    .transform(CONVERT_FROM_REST)
                    .toList();
        } catch (IOException e) {
            // TODO handle exceptions (cache?)
            log.error("Failed to retrieve pull requests", e);
            return Collections.emptyList();
        }
    }
}
