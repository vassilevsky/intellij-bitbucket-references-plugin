package com.atlassian.bitbucket.idea.pullrequest;

import com.atlassian.bitbucket.idea.pullrequest.change.ChangedFile;
import com.atlassian.bitbucket.idea.pullrequest.change.ChangedFilesProvider;
import com.atlassian.bitbucket.idea.pullrequest.comment.CommentsProvider;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * IntelliJ project service for Pull Request
 */
// TODO javadoc on methods
public interface PullRequestService {

    @Nullable
    PullRequest getActivePullRequest();

    void setActivePullRequest(@Nullable PullRequest pullRequest);

    @Nullable
    ChangedFilesProvider getChangedFileProvider();

    @Nullable
    CommentsProvider getCommentsProvider(ChangedFile changedFile);

    List<PullRequest> getOpenPullRequests(@NotNull Repository repository);

    /**
     * @return relative path to the file from root of the project
     */
    @NotNull
    String resolveRelativePath(VirtualFile virtualFile);
}
