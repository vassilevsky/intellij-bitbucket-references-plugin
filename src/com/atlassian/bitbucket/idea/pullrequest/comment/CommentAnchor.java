package com.atlassian.bitbucket.idea.pullrequest.comment;

public interface CommentAnchor {

    enum FileType {
        FROM, TO
    }

    enum LineType {
        ADDED, CONTEXT, REMOVED
    }

    FileType getFileType();

    LineType getLineType();

    int getLineNumber();
}
