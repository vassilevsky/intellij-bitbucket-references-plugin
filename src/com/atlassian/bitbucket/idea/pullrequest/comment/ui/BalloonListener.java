package com.atlassian.bitbucket.idea.pullrequest.comment.ui;


import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.util.messages.Topic;

public interface BalloonListener {

    Topic<BalloonListener> BALLOON_TOPIC = Topic.create("balloon action", BalloonListener.class);

    void showBalloonRequested(Balloon balloon);

}
