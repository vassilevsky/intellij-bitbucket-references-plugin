package com.atlassian.bitbucket.idea.pullrequest.comment.ui;

import com.intellij.openapi.editor.impl.EditorComponentImpl;

import javax.swing.border.Border;
import java.awt.*;

public class BackgroundImage implements Border {
    private Image image;

    public BackgroundImage(Image image) {
        this.image = image;
    }

    public void paintBorder(Component component, Graphics graphics, int x, int y, int width, int height) {
        if (image == null) {
            return;
        }

        if (!(component instanceof EditorComponentImpl)) {
            return;
        }

        EditorComponentImpl editor = (EditorComponentImpl) component;

        Graphics2D graphics2 = (Graphics2D) graphics;
        graphics2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));

        Point p = calculatePosition(editor, image.getWidth(null), image.getHeight(null));
        graphics2.drawImage(image, p.x, p.y, editor);
    }

    private Point calculatePosition(EditorComponentImpl editor, int imageWidth, int imageHeight) {
        Point p = new Point();

        Rectangle rect = editor.getVisibleRect();
        p.setLocation(
                rect.x + (rect.width - imageWidth) / 2,
                rect.y + (rect.height - imageHeight) / 2);

        return p;
    }

    public Insets getBorderInsets(Component c) {
        return new Insets(0, 0, 0, 0);
    }

    public boolean isBorderOpaque() {
        return true;
    }
}