package com.atlassian.bitbucket.idea.pullrequest.comment;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface CommentsProvider {

    @NotNull
    List<Comment> getComments();

    @NotNull
    List<Comment> getComments(int lineNumber);

    void replyToComment(@NotNull Comment comment, String text);
}
