package com.atlassian.bitbucket.idea.pullrequest;

import java.net.URI;

public class DefaultUser implements User {

    private final String displayName;
    private final String slug;
    private final URI userUri;

    private DefaultUser(Builder builder) {
        displayName = builder.displayName;
        slug = builder.slug;
        userUri = builder.userUri;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getSlug() {
        return slug;
    }

    @Override
    public URI getUserUrl() {
        return userUri;
    }

    @Override
    public URI getAvatarUrl(int avatarSize) {
        if (avatarSize < 0 || avatarSize > 256) {
            avatarSize = 32;
        }
        return userUri.resolve(String.format("/avatar.png?s=%d", avatarSize));
    }

    public static final class Builder {
        private String displayName;
        private String slug;
        private URI userUri;

        private Builder() {
        }

        public Builder displayName(String displayName) {
            this.displayName = displayName;
            return this;
        }

        public Builder slug(String slug) {
            this.slug = slug;
            return this;
        }

        public Builder userUri(URI userUri) {
            this.userUri = userUri;
            return this;
        }

        public DefaultUser build() {
            return new DefaultUser(this);
        }
    }
}
