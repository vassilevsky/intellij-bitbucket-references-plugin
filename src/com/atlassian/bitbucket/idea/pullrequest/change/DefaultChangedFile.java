package com.atlassian.bitbucket.idea.pullrequest.change;

import com.atlassian.bitbucket.idea.pullrequest.comment.Comment;
import com.atlassian.bitbucket.idea.pullrequest.comment.CommentsProvider;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public class DefaultChangedFile implements ChangedFile {
    private final int activeCommentsNumber;
    private final ChangeType changeType;
    private final String name;
    private final int orphanedCommentsNumber;
    private final String relativePath;

    private DefaultChangedFile(Builder builder) {
        activeCommentsNumber = builder.activeCommentsNumber;
        changeType = builder.changeType;
        name = builder.name;
        orphanedCommentsNumber = builder.orphanedCommentsNumber;
        relativePath = builder.relativePath;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public int getActiveCommentsNumber() {
        return activeCommentsNumber;
    }

    @Override
    public ChangeType getChangeType() {
        return changeType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getOrphanedCommentsNumber() {
        return orphanedCommentsNumber;
    }

    @Override
    public String getRelativePath() {
        return relativePath;
    }


    public static final class Builder {
        private int activeCommentsNumber;
        private ChangeType changeType;
        private String name;
        private int orphanedCommentsNumber;
        private String relativePath;

        private Builder() {
        }

        public Builder activeCommentsNumber(int activeCommentsNumber) {
            this.activeCommentsNumber = activeCommentsNumber;
            return this;
        }

        public Builder changeType(@NotNull ChangedFile.ChangeType changeType) {
            this.changeType = changeType;
            return this;
        }

        public Builder name(@NotNull String name) {
            this.name = name;
            return this;
        }

        public Builder orphanedCommentsNumber(int orphanedCommentsNumber) {
            this.orphanedCommentsNumber = orphanedCommentsNumber;
            return this;
        }

        public Builder relativePath(@NotNull String relativePath) {
            this.relativePath = relativePath;
            return this;
        }

        public DefaultChangedFile build() {
            return new DefaultChangedFile(this);
        }
    }
}
