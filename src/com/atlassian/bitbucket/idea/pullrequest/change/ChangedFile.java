package com.atlassian.bitbucket.idea.pullrequest.change;

public interface ChangedFile {

    enum ChangeType {
        ADD,
        DELETE,
        MODIFY
    }

    int getActiveCommentsNumber();

    ChangeType getChangeType();

    String getName();

    int getOrphanedCommentsNumber();

    String getRelativePath();
}
