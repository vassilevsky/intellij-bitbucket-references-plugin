package com.atlassian.bitbucket.idea.pullrequest.action;

import com.atlassian.bitbucket.idea.pullrequest.PullRequestService;
import com.atlassian.bitbucket.idea.pullrequest.rest.PullRequestRestApi;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.Presentation;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.IconLoader;

import javax.swing.*;
import java.io.IOException;

public class ApproveAction extends DumbAwareAction {
    private static final Icon APPROVE_ICON = IconLoader.getIcon("/icons/approve.png");
    private static final Icon UNAPPROVE_ICON = IconLoader.getIcon("/icons/unapprove.png");

    private boolean approved = false;


    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getProject();
        PullRequestService pullRequestService = ServiceManager.getService(project, PullRequestService.class);
        PullRequestRestApi pullRequestRestApi = ServiceManager.getService(project, PullRequestRestApi.class);

        if (approved) {
            try {
                if (pullRequestRestApi.unapprove(pullRequestService.getActivePullRequest().getId())) {
                    toggleStatus();
                }
            } catch (IOException e) {
                // TODO
                e.printStackTrace();
            }
        } else {
            try {
                if (pullRequestRestApi.approve(pullRequestService.getActivePullRequest().getId())) {
                    toggleStatus();
                }
            } catch (IOException e) {
                // TODO
                e.printStackTrace();

            }
        }
    }

    private void toggleStatus() {
        approved = !approved;
    }

    @Override
    public void update(AnActionEvent event) {
        Project project = event.getProject();
        Presentation presentation = event.getPresentation();

        if (project != null) {
            PullRequestService pullRequestService = ServiceManager.getService(project, PullRequestService.class);
            if (pullRequestService != null && pullRequestService.getActivePullRequest() != null) {

                if (approved) {
                    presentation.setIcon(UNAPPROVE_ICON);
                    presentation.setText("Unapprove");
                    presentation.setDescription("Remove your approval of the Pull Request");
                } else {
                    presentation.setIcon(APPROVE_ICON);
                    presentation.setText("Approve");
                    presentation.setDescription("Approve the Pull Request");
                }

                presentation.setEnabledAndVisible(true);
                return;
            }
        }
        presentation.setEnabledAndVisible(false);
    }
}
