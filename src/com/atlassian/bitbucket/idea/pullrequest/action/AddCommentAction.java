package com.atlassian.bitbucket.idea.pullrequest.action;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.idea.pullrequest.PullRequestService;
import com.atlassian.bitbucket.idea.pullrequest.rest.PullRequestRestApi;
import com.atlassian.bitbucket.idea.pullrequest.rest.RestDiff;
import com.atlassian.bitbucket.idea.pullrequest.rest.RestPostComment;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.LogicalPosition;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.io.IOException;
import java.util.List;

import static com.atlassian.bitbucket.idea.pullrequest.rest.RestDiff.Line;
import static com.atlassian.bitbucket.idea.pullrequest.rest.RestDiff.Segment;

public class AddCommentAction extends DumbAwareAction {
    private static final Logger log = Logger.getInstance(Constants.LOGGER_NAME);

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getProject();
        VirtualFile file = CommonDataKeys.VIRTUAL_FILE.getData(event.getDataContext());
        Editor editor = CommonDataKeys.EDITOR.getData(event.getDataContext());
        LogicalPosition currentLogicalPosition = getCurrentLogicalPosition(event);
        PullRequestService pullRequestService = ServiceManager.getService(project, PullRequestService.class);
        PullRequestRestApi pullRequestRestApi = ServiceManager.getService(project, PullRequestRestApi.class);
        String lineType = getLineType(event);

        String text = (String) JOptionPane.showInputDialog(
                editor.getComponent(),
                "Enter comment text:",
                "Post comment",
                JOptionPane.QUESTION_MESSAGE,
                Constants.BITBUCKET_ICON,
                null,
                null);

        if ((text != null) && (text.length() > 0)) {
            try {

                pullRequestRestApi.postComment(
                        pullRequestService.getActivePullRequest().getId(),
                        RestPostComment.create(
                                currentLogicalPosition.line + 1,
                                lineType,
                                pullRequestService.resolveRelativePath(file),
                                text));
                // TODO invoke redraw
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(AnActionEvent event) {
        Project project = event.getProject();

        LogicalPosition currentLogicalPosition = getCurrentLogicalPosition(event);
        if (currentLogicalPosition != null) {
            PullRequestService pullRequestService = ServiceManager.getService(project, PullRequestService.class);
            if (pullRequestService != null && pullRequestService.getActivePullRequest() != null) {
                if (getLineType(event) != null) {
                    event.getPresentation().setEnabledAndVisible(true);
                    return;
                }
            }
        }

        event.getPresentation().setEnabledAndVisible(false);
    }

    @Nullable
    private LogicalPosition getCurrentLogicalPosition(AnActionEvent event) {
        Project project = event.getProject();
        if (project != null) {
            VirtualFile file = CommonDataKeys.VIRTUAL_FILE.getData(event.getDataContext());
            if (file != null) {
                Editor editor = CommonDataKeys.EDITOR.getData(event.getDataContext());
                if (editor != null) {
                    CaretModel caretModel = editor.getCaretModel();
                    if (caretModel.getCaretCount() == 1) {
                        Caret caret = caretModel.getPrimaryCaret();
                        return caret.getLogicalPosition();
                    }
                }
            }
        }
        return null;
    }

    @Nullable
    private String getLineType(AnActionEvent event) {
        Project project = event.getProject();
        VirtualFile file = CommonDataKeys.VIRTUAL_FILE.getData(event.getDataContext());
        LogicalPosition currentLogicalPosition = getCurrentLogicalPosition(event);
        PullRequestService pullRequestService = ServiceManager.getService(project, PullRequestService.class);
        PullRequestRestApi pullRequestRestApi = ServiceManager.getService(project, PullRequestRestApi.class);

        RestDiff diff;
        try {
            diff = pullRequestRestApi.getDiff(
                    pullRequestService.getActivePullRequest().getId(),
                    pullRequestService.resolveRelativePath(file));
        } catch (IOException e) {
            log.error("Failed to get diff");
            return null;
        }

        List<Segment> allSegments = diff.getAllSegments();

        for (Segment segment : allSegments) {
            for (Line line : segment.getLines()) {
                if (currentLogicalPosition.line + 1 == line.getDestination()) {
                    return segment.getType();
                }
            }
        }

        return null;
    }
}
