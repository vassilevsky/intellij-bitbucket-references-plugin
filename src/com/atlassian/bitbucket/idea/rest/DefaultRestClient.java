package com.atlassian.bitbucket.idea.rest;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.idea.configuration.BitbucketAuthentication;
import com.atlassian.bitbucket.idea.configuration.BitbucketSettingsService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.intellij.openapi.diagnostic.Logger;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URI;

public class DefaultRestClient implements RestClient {
    private static final Logger log = Logger.getInstance(Constants.LOGGER_NAME);
    private static final int CONNECTION_TIMEOUT = 5000;

    private final URI baseServerUri;
    private final BitbucketSettingsService settingService;

    public DefaultRestClient(URI baseServerUri, BitbucketSettingsService settingService) {
        this.baseServerUri = baseServerUri;
        this.settingService = settingService;
        initSSLCertPolicy();
    }

    @Override
    public JsonObject get(String path) throws IOException {
        URI requestUri = baseServerUri.resolve(path);
        final GetMethod method = new GetMethod(requestUri.toString());

        executeHttpMethod(settingService.getAuthentication(), method);

        JsonParser parser = new JsonParser();
        String response = method.getResponseBodyAsString();

        try {
            return parser.parse(response).getAsJsonObject();
        } catch (JsonSyntaxException e) {
            log.error(String.format("Failed to parse response from server: '%s'", response), e);
            throw new IOException("Failed to parse response from server", e);
        }
    }

    @Override
    public int post(String path, String jsonBody) throws IOException {
        URI requestUri = baseServerUri.resolve(path);
        StringRequestEntity requestEntity = new StringRequestEntity(jsonBody, "application/json", "UTF-8");

        PostMethod method = new PostMethod(requestUri.toString());
        method.setRequestEntity(requestEntity);

        executeHttpMethod(settingService.getAuthentication(), method);

        // TODO error handling?
        return method.getStatusCode();
    }

    public int delete(String path) throws IOException {
        URI requestUri = baseServerUri.resolve(path);

        DeleteMethod method = new DeleteMethod(requestUri.toString());

        executeHttpMethod(settingService.getAuthentication(), method);

        // TODO error handling?
        return method.getStatusCode();
    }

    private void executeHttpMethod(BitbucketAuthentication settings, @NotNull HttpMethodBase method) throws IOException {
        method.addRequestHeader(settings.getAuthHeader());
        method.addRequestHeader(new Header("accept", "application/json"));

        final HttpClient client = new HttpClient();
        HttpConnectionManagerParams params = client.getHttpConnectionManager().getParams();
        params.setConnectionTimeout(CONNECTION_TIMEOUT); //set connection timeout (how long it takes to connect to remote host)
        params.setSoTimeout(CONNECTION_TIMEOUT); //set socket timeout (how long it takes to retrieve data from remote host)
        client.executeMethod(method);
    }

    private void initSSLCertPolicy() {
        EasySSLProtocolSocketFactory secureProtocolSocketFactory = new EasySSLProtocolSocketFactory();
        Protocol.registerProtocol("https", new Protocol("https", (ProtocolSocketFactory) secureProtocolSocketFactory, 443));
    }
}
