package com.atlassian.bitbucket.idea.configuration;

import org.jetbrains.annotations.NotNull;

public interface BitbucketSettingsService {

    @NotNull
    BitbucketAuthentication getAuthentication();

    void saveAuthentication(BitbucketAuthentication authentication);
}
