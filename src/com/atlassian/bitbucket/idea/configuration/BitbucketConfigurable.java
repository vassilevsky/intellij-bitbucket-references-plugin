package com.atlassian.bitbucket.idea.configuration;

import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;
import com.intellij.openapi.util.text.StringUtil;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/*
 * This file was derived from code in https://github.com/ktisha/Crucible4IDEA/
 *
 * Copyright (c) 2013-2015 Ekaterina Tuzova (ktisha)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */
public class BitbucketConfigurable implements SearchableConfigurable {

    private JPanel mainPanel;
    private JTextField usernameField;
    private JPasswordField passwordField;

    private final BitbucketSettingsService settingsService;

    public BitbucketConfigurable() {
        settingsService = DefaultBitbucketSettingsService.getInstance();
    }

    private void saveSettings() {
        BitbucketAuthentication settings = new DefaultBitbucketAuthentication(usernameField.getText(), new String(passwordField.getPassword()));
        settingsService.saveAuthentication(settings);
    }

    @NotNull
    @Override
    public String getId() {
        return BitbucketConfigurable.class.getSimpleName();
    }

    @Nullable
    @Override
    public Runnable enableSearch(String option) {
        return null;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Bitbucket";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        return mainPanel;
    }

    @Override
    public boolean isModified() {
        final BitbucketAuthentication oldAuth = settingsService.getAuthentication();

        return !StringUtil.equals(oldAuth.getUsername(), usernameField.getText()) ||
                !StringUtil.equals(oldAuth.getPassword(), new String(passwordField.getPassword()));
    }

    @Override
    public void apply() throws ConfigurationException {
        saveSettings();
    }

    @Override
    public void reset() {
        BitbucketAuthentication auth = settingsService.getAuthentication();
        usernameField.setText(auth.getUsername());
        passwordField.setText(auth.getPassword());
    }

    @Override
    public void disposeUIResources() {
    }
}
