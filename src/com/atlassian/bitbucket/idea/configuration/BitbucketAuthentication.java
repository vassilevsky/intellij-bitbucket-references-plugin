package com.atlassian.bitbucket.idea.configuration;

import org.apache.commons.httpclient.Header;

public interface BitbucketAuthentication {
    Header getAuthHeader();

    String getUsername();

    String getPassword();
}
