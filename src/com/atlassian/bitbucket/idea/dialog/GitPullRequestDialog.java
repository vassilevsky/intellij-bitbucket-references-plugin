package com.atlassian.bitbucket.idea.dialog;

import com.atlassian.bitbucket.idea.pullrequest.PullRequest;
import com.atlassian.bitbucket.idea.pullrequest.PullRequestService;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.table.JBTable;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;

/*
 * This file was derived from code in https://github.com/ktisha/Crucible4IDEA/
 *
 * Copyright (c) 2013-2015 Ekaterina Tuzova (ktisha)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */
public class GitPullRequestDialog extends DialogWrapper {

    private JPanel mainPanel;
    private JScrollPane pullRequestScrollPane;
    private JBTable pullRequestTable;

    private final PullRequestTableModel pullRequestTableModel;
    private final TableRowSorter<TableModel> pullRequestTableSorter;

    private PullRequest selectedPullRequest;

    public GitPullRequestDialog(Project project, PullRequestService pullRequestService, Repository repository) {
        super(project, true);

        setTitle("Review Pull Request");

        pullRequestTableModel = new PullRequestTableModel(pullRequestService, repository);
        pullRequestTable.setModel(pullRequestTableModel);
        pullRequestTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pullRequestTable.setStriped(true);
        pullRequestTable.setExpandableItemsEnabled(false);

        pullRequestScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        final TableColumnModel columnModel = pullRequestTable.getColumnModel();
        columnModel.getColumn(0).setMinWidth(50);
        columnModel.getColumn(0).setMaxWidth(50);
        columnModel.getColumn(1).setMinWidth(200);
        columnModel.getColumn(1).setPreferredWidth(200);
        columnModel.getColumn(2).setMinWidth(100);
        columnModel.getColumn(2).setMaxWidth(100);
        columnModel.getColumn(3).setMinWidth(100);
        columnModel.getColumn(3).setMaxWidth(100);

        pullRequestTableSorter = new TableRowSorter<TableModel>(pullRequestTableModel);
        pullRequestTableSorter.setSortKeys(Collections.singletonList(new RowSorter.SortKey(3, SortOrder.ASCENDING)));
        pullRequestTableSorter.sort();
        pullRequestTable.setRowSorter(pullRequestTableSorter);

        pullRequestTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    updatePullRequestSelection();
                    clickDefaultButton();
                }
            }
        });

        pullRequestTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                updatePullRequestSelection();
            }
        });

        updatePullRequestSelection();

        init();
    }

    public PullRequest getSelectedPullRequest() {
        return selectedPullRequest;
    }

    protected JComponent createCenterPanel() {
        return mainPanel;
    }

    @Override
    protected String getDimensionServiceKey() {
        return getClass().getName();
    }

    @Override
    public JComponent getPreferredFocusedComponent() {
        return pullRequestScrollPane;
    }

    private void updatePullRequestSelection() {
        final int sortedRowIndex = pullRequestTable.getSelectedRow();
        if (sortedRowIndex >= 0 && sortedRowIndex < pullRequestTable.getRowCount()) {
            final int modelIndex = pullRequestTableSorter.convertRowIndexToModel(sortedRowIndex);
            selectedPullRequest = pullRequestTableModel.getPullRequest(modelIndex);
        } else {
            selectedPullRequest = null;
        }

        setOKActionEnabled(selectedPullRequest != null);
    }

}
