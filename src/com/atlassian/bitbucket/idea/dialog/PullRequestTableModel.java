package com.atlassian.bitbucket.idea.dialog;

import com.atlassian.bitbucket.idea.pullrequest.PullRequest;
import com.atlassian.bitbucket.idea.pullrequest.PullRequestService;
import com.intellij.dvcs.repo.Repository;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

public class PullRequestTableModel extends AbstractTableModel {

    private final Repository repository;
    private final PullRequestService pullRequestService;

    private List<PullRequest> openPullRequests;

    public PullRequestTableModel(PullRequestService pullRequestService, Repository repository) {
        this.repository = repository;
        this.pullRequestService = pullRequestService;
        updateData();
    }

    @Override
    public int getColumnCount() {
        return Columns.values().length;
    }

    @Override
    public String getColumnName(int column) {
        return Columns.values()[column].name;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        return Columns.values()[column].clazz;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public int getRowCount() {
        return openPullRequests.size();
    }

    public PullRequest getPullRequest(int row) {
        return openPullRequests.get(row);
    }

    private void updateData() {
        openPullRequests = pullRequestService.getOpenPullRequests(repository);
        fireTableDataChanged();
    }

    @Override
    public Object getValueAt(int row, int column) {
        PullRequest pr = getPullRequest(row);
        return Columns.values()[column].valueFunction.apply(pr);
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        throw new UnsupportedOperationException("setValueAt");
    }

    private enum Columns {
        ID("ID", Integer.class, PullRequest::getId),
        DESCRIPTION("Title", String.class, PullRequest::getTitle),
        AUTHOR("Author", String.class, pullRequest -> pullRequest.getAuthor().getDisplayName()),
        DATE("Created", Date.class, PullRequest::getCreatedDate);

        private final String name;
        private final Class<?> clazz;
        private final Function<PullRequest, Object> valueFunction;

        Columns(String description, Class<?> clazz, Function<PullRequest, Object> valueFunction) {
            this.name = description;
            this.clazz = clazz;
            this.valueFunction = valueFunction;
        }
    }
}
