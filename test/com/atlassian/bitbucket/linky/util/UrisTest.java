package com.atlassian.bitbucket.linky.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UrisTest {

    @Test
    public void testQuote() {
        String actual = Uris.quote("branch#5><|.hello!&])({}@.@$&+_-`'\"");
        assertEquals("branch%235%3E%3C%7C.hello%21%26%5D%29%28%7B%7D%40.%40%24%26%2B_-%60%27%22", actual);
    }
}
